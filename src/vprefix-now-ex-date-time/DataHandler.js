import moment from 'moment-timezone';
import { dateUpdatedEventName, datetimePickerElement } from './constants.js';
        
export default class DataHandler {
    
    constructor(root, shadowRoot, helpers, state) {

        console.log(`in DateTime constructor`, state.properties);

        this._root = root

        this._shadowRoot = shadowRoot
        this._dispatch = helpers.dispatch
        this._updateState = helpers.updateState
        this._properties = state.properties
        this._state = state

        this._loadThirdPartLib();
    }   
    
    _loadThirdPartLib() {
        
        const css = document.createElement('link');
        css.rel = 'stylesheet';
        css.type = 'text/css'
        css.href = 'https://cdn.dhtmlx.com/suite/7.3/suite.js';
        this._shadowRoot.appendChild(css);

        this._scriptLib = document.createElement('script');
        this._scriptLib.type = 'text/javascript';
        this._scriptLib.src = 'https://cdn.dhtmlx.com/suite/7.3/suite.css';
        this._shadowRoot.appendChild(this._scriptLib);

        this._scriptLib.addEventListener('load', this._initialTabDef.bind(this));          
    }
    
    _initialTabDef() {

        try {
            const self = this;

            const dateInput = this._shadowRoot.querySelector("#date-input");

            // init calendar without container, use null instead of container
            this._calendar = new dhx.Calendar(dateInput, {
                timePicker: true,
                css: "dhx_widget--bordered"
            });
            
            // when calendar value changed, it trigger update input value and hide popup
            this._calendar.events.on("change", function () {

                if (!_settingDate) {
                    const date = self._calendar.getValue(true);
                    const value = moment(date).format(self._properties.dataFormat);      
                    const dispValue = moment(date).format(self._properties.displayFormat);      
                    const data = { value: value };

                    console.log(`GET DATE [${value}] Event[${dateUpdatedEventName}]`);
                    
                    if (self._properties && self._properties.name) {
                        data.name = self._properties.name;
                    }
                    
                    self._dispatch(dateUpdatedEventName, data);

                    self._updateState({ edit: false, value: dispValue, error: '' });
                }
            });

            document.addEventListener("click", (evt) => {

                const showing = this._root.querySelector(".show");

                console.log(`Datepicker [${datetimePickerElement}] click [${showing}]`, evt.path);

                if (showing) {

                    const flyoutElement = dateInput;
                    let targetElement = evt.target; // clicked element
                    
                    // Note: when a button is clicked in the datepicker the targetElement appears as main macroponent
                    //       need to check path to determine if in datepicker

                    console.log(`Checking for datepicker [${datetimePickerElement}]`, evt.path);

                    const datepicker = evt.path.find((e) => { 
                            return (e.tagName && e.tagName.toLowerCase().startsWith(datetimePickerElement));
                        });

                    if (datepicker) {
                        // path includes the date-picker so just return, don't close
                        return
                    }

                    do {
                        console.log(`Check`, targetElement) 
                        if (targetElement == flyoutElement) {
                            // This is a click inside. Do nothing, just return.
                            return;
                        }
                        // Go up the DOM
                        targetElement = targetElement.parentNode;
                    } while (targetElement);            

                    // This is a click outside so close the popup          
                    self._updateState({ edit: false });
                }
            });
        }
        catch (error) {
            console.error(`Error [init-date] ${error}`);
        }
    }

    setDate(date) {
        _settingDate = true;

        console.log(`SETTING DATE`, date);
        this._calendar.setValue(date)

        setTimeout(() => {
            _settingDate = false;
        }, 200);
    }
}

let _settingDate = false;

