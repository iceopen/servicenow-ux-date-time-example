import '@servicenow/now-card';
import moment from 'moment-timezone'
import DataHandler from './DataHandler';

function bootstrap(element, helpers, state) {

	try {
		const {dispatch, updateState, updateProperties} = helpers;

		console.log(`BOOTSTRAP LOADING DATE-TIME ELEMENT`);

		let tmpl = document.createElement('template');
		tmpl.innerHTML = `<div id="date-input"></div>`;

		const shadow = element.attachShadow({mode: 'open'});
		shadow.appendChild(tmpl.content.cloneNode(true));

		const root = element.getRootNode();

		const handler = new DataHandler(root, shadow, helpers, state);

		// save handler and element in state
		updateState({ dataObject: handler, dataObjectElement: element, rootElement: root });

		const { value, error, dataFormat, displayFormat } = state.properties;

		if (value) {
		    const m = moment(value, dataFormat);

		    console.log(`DATE-TIME Prop Load - value[${value}] formatted[${m.format(displayFormat)}]`)

		    // save passed value in state
		    updateState({ value: m.format(displayFormat), error: error });
		}
		else {
		    updateState({ value: '' });
		}

	}
	catch (error) {
		console.warn(`Error [bootstrap]: ${error}`);
	}
}

export default (state, helpers) => {
	const {  } = state;
	const {dispatch, updateState, updateProperties} = helpers;

	let view;

	// create view - with hook on picker to allow bootstrap of element
	try {
		view = (
			<div className="now-ex-date-time">	
				<div className="now-ex-date-time__input">
	 				<now-input 
					 	placeholder="Date/Time" 
						type="text" 
						value={state.value} 
						componentName="datetime-input" 
						label={state.properties.label}
						messages={getErrorMessages(state.error)}
					>
	 					<now-button-iconic slot="end" icon="calendar-clock-outline" componentName="datetime-icon" size="sm" bare></now-button-iconic>
	 				</now-input>
	 			</div>
				 
				 <div ></div>
				<now-ex-date-time-picker className={ `${state.edit ? 'show' : ''}` }
					hook-insert={ (vnode) => {
						updateState({ rootElement: vnode.elm })
						console.log('DATE-TIME Element Inserted', vnode.elm)
						bootstrap(vnode.elm, helpers, state)
				}}></now-ex-date-time-picker>
			</div>
		);
	}
	catch (error) {
		console.warn(`DATE-TIME VIEW ERROR [${error}]`);
	}

	console.log(`DATE-TIME end view`);
	
	return view;
};

function getErrorMessages(error) {

	if (error) {
		console.log(`DATE-TIME got error data`)
		return [ { status: "critical", content: error }]
	}
	return []
}
