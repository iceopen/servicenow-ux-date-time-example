import {createCustomElement} from '@servicenow/ui-core';
import snabbdom from '@servicenow/ui-renderer-snabbdom';
import styles from './styles.scss';
import view from './view';
import actionHandlers from './actionHandlers'

createCustomElement('icefl-now-ex-date-time', {
	renderer: {type: snabbdom},
	view,
	properties: {
		label: {
			default: ''
		},
		name: {
			default: ''
		},
		value: {
			default: ''
		},
		dataFormat: {
			default: 'YYYY-MM-DD HH:mm:ss'
		},
		displayFormat: {
			default: 'HH:mm DD-MMM-YY'
		},
		error: {
			default: ''
		}
	},
	styles,
	initialState: {	
		value: '',	
		error: '',	
		edit: false
	},
	actionHandlers
});
