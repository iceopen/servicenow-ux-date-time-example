import { actionTypes } from '@servicenow/ui-core';
import moment from 'moment-timezone'
import { dateUpdatedEventName } from './constants.js';

const { COMPONENT_BOOTSTRAPPED, COMPONENT_PROPERTY_CHANGED, COMPONENT_ERROR_THROWN, COMPONENT_DISCONNECTED } = actionTypes;

export default {
    [COMPONENT_BOOTSTRAPPED]: (coeffects) => {        

        try {
            const { dispatch, updateState, properties, state } = coeffects;
            const { value, error } = properties;

            console.log(`DATE-TIME COMPONENT BOOTSTRAP`);
        }
        catch (error) {
            console.warn(`Error [date-time-bootstap]: ${error}`);
        }        
	},
    [COMPONENT_DISCONNECTED]: ({ action, updateState, dispatch, state }) => {    
        console.log(`DATE-TIME DISCONNECTED`);
    },
    [COMPONENT_PROPERTY_CHANGED]: ({ action, updateState, properties, dispatch, state }) => {    
        try {    
            console.log(`DATE-TIME Prop Changed`)

            if (action && action.payload) {
                const {name, value} = action.payload;

                console.log(`DATE-TIME Prop Changed: name[${name}] value[${value}] prop[${state.properties.name}]`)

                if (name === 'value') {

                    if (value) {
                        try {
                            const m = moment(value, properties.dataFormat);
                        
                            updateState({ value: m.format(properties.displayFormat) });
                        }
                        catch (error) {

                        }
                    }
                    else {
                        updateState({ value: '' });
                    }
                }
                else
                if (name === 'error') {
                    updateState({ error: value });
                }
            }
        }
        catch (error) {
            console.warn(`DATE-TIME Prop Changed error[${error}]`)
        }
	},
    [COMPONENT_ERROR_THROWN]: ({action, state}) => {        
        try {
            console.error(`In Error Thrown [${JSON.stringify(action)}] [${JSON.stringify(state)}]`)
        }
        catch (error) {
            console.error(`DATE-TIME error thrown[${error}]`)
        }
	},

    'NOW_BUTTON_ICONIC#CLICKED': ({ action, dispatch, properties, updateState, state }) => {

        try {
            const { dataObject } = state;
            const { componentName, id } = action.meta;

            try {
                const m = moment(state.value, properties.displayFormat);
                const date = m.toDate();

                console.log(`INPUT DATE [${state.value}]`, date)

                dataObject.setDate(date);
            }
            catch (error) {
                // default to now
                dataObject.setDate(new Date());
            }

            updateState({ edit: true });
        }
        catch (error) {
            console.warn(`Error [date-time-iconic]: ${error}`)
        }
    },
    'NOW_INPUT#VALUE_SET': ({ action, dispatch, properties, updateState, state, updateProperties }) => {

        try {
            const { dataObject } = state;
            const { value } = action.payload;
            const { componentName, id } = action.meta;

            try {                
                const m = moment(value, properties.displayFormat);

                const d = m.format(properties.dataFormat);

                console.log(`INPUT DATE ENTERED[${value}] moment[${d}] format[${properties.dataFormat}]`)

                if (d.toLowerCase() !== 'invalid date') {

                    const data = { value: d };

                    console.log(`GET DATE [${value}] Event[${dateUpdatedEventName}]`);
                    
                    if (properties && properties.name) {
                        data.name = properties.name;
                    }
                    
                    dispatch(dateUpdatedEventName, data);

                    updateState({ value: value, error: '' });
                    updateProperties({ error: '' });

                    // set picker date for view/change
                    const date = m.toDate();

                    console.log(`INPUT DATE [${value}]`, date)

                    dataObject.setDate(date);
                }
                else {
                    if (value === "") {
                        if (!properties.optional) {
                            updateState({ error: 'Required' });
                        }
                    }
                    else {
                        updateState({ error: d });
                    }
                }

            }
            catch (error) {
                console.warn(`DATE Error [${error}]`)
                // default to now
                dataObject.setDate(new Date());
            }
        }
        catch (error) {
            console.warn(`Error [date-time-iconic]: ${error}`)
        }
    }

}