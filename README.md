# Example Date/Time Picker Component built for Now Experience UI Framework using DHTMLX Suite Calendar

Note: This is an example project and is for use as is.  

## 1. Setup

Once cloned from git you will need to build the project via:

    ```
    npm install
    ```

This will install all the required components for local npm

## 2. Changes Required

To deploy this to your instance you will need to make a couple of changes 

1.  The deployment requires components to use your vendor prefix (with or without the 'x_' part)

    So you will need to change this in the following:

    - `now-ui.json`
        - component name
        - scope name

    - Folder name in `/src`

    - `/src/index.js` to refer to folder above

    - in `<xxxx>-now-ex-date-time/index.js`


2.  There a couple of field names which you may want to configure stored in `constants.js`

    - `dateUpdatedEventName` is the name of the event which is dispatched when a date/time is updated

    - `datetimePickerElement` is the name of picker html element in `view.js`.  If changed you will need to align these values.

